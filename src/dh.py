#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable= too-many-lines
r'''
  _____                        _       _    _ _ _            _
 |  __ \                      (_)     | |  | (_|_)          | |
 | |  | | ___  _ __ ___   __ _ _ _ __ | |__| |_ _  __ _  ___| | __
 | |  | |/ _ \| '_ ` _ \ / _` | | '_ \|  __  | | |/ _` |/ __| |/ /
 | |__| | (_) | | | | | | (_| | | | | | |  | | | | (_| | (__|   <
 |_____/ \___/|_| |_| |_|\__,_|_|_| |_|_|  |_|_| |\__,_|\___|_|\_\\
                                              _/ |
                                             |__/
    /////////     Search hijackable domain names     \\\\\\\\\\\\\\\
   /////////            with github pages             \\\\\\\\\\\\\\\
  /////////           Made with ❤ by A1CY0N            \\\\\\\\\\\\\\\

Usage:
    dh.py [-hvx] [-d <METHOD>] [-l <LOG_LEVEL>] [-f <COUNTRY>] [-c <CONF_FILE>] [-i <IN_FILE>] [-o <OUT_FILE>]

Arguments:
  LOG_LEVEL        log level
  CONF_FILE        optional configuration file
  METHOD           Method for retrieving domains
  IN_FILE          optional file which contains 1 domain per line
  OUT_FILE         optional file with hijackable domains
  COUNTRY          country of origin of the domain

Examples:
  dh.py -i domains.list -o hijackable.list    # Write hijackable domains from in file to out file.
  dh.py -xc /home/test/config.txt -f Germany  # Get only hijackable German domains

Options:
    -h --help               Help
    -v --version            DomainHijack version
    -l --log-level=<level>  Log level (CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET) [default: CRITICAL]
    -d --domains=<method>   Method for getting domains (github, file, reverse_lookup, all) [default: all]
    -f --flag=<country>     Search only the domains of a country
    -x --only-custom        Not displaying github.io subdomains
    -i --in-file=<file>     File with list of domains to check
    -o --out-file=<file>    File with list of hijackable domains
    -c --conf-file=<file>   Custom configuration file [default: settings.ini]
'''
# Standard imports
import re
import sys
import random
import logging
import os.path
from queue import Queue
from threading import Thread
from collections import OrderedDict
from configparser import SafeConfigParser
from urllib3.util import Retry  # type: ignore
import requests
from requests import ConnectionError  # pylint: disable=redefined-builtin
from requests.adapters import HTTPAdapter

# Project import
import whois  # type: ignore
import pydig  # type: ignore
import tldextract  # type: ignore
from github import Github
from github import RateLimitExceededException
from lib.argumentsParsing.docopt import docopt  # type: ignore

VERSION = 'DomainHijack 0.1.3'
BANNER = r'''
  _____                        _       _    _ _ _            _
 |  __ \                      (_)     | |  | (_|_)          | |
 | |  | | ___  _ __ ___   __ _ _ _ __ | |__| |_ _  __ _  ___| | __
 | |  | |/ _ \| '_ ` _ \ / _` | | '_ \|  __  | | |/ _` |/ __| |/ /
 | |__| | (_) | | | | | | (_| | | | | | |  | | | | (_| | (__|   <
 |_____/ \___/|_| |_| |_|\__,_|_|_| |_|_|  |_|_| |\__,_|\___|_|\_\\
                                              _/ |
                                             |__/
 ___
/,_ \    _,      Search for hijackable domain with github pages
|/ )/   / |
  //  _/  |
 / ( /   _)                   Written by A1CYON
/   `   _/)                https://gitlab.com/A1CY0N
\  ~=-   /,
^~^~^~^~^~^~^~'''
MENU = r'''
Warning!!! The search for vulnerable domains can really take time.
You can significantly reduce processing time by checking only custom
domains, specifying the method to retrieve domains or testing only
the hijacking of domains (not subdomains).

********************  DomainHijack Main Menu  ********************
1) Find vulnerable domains
2) Generate a CNAME file
3) Exit
'''


class GetDomains:
    """
        .. module:: dh
        .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

        :platform: Unix, Windows
        :synopsis: Provides methods for retrieving a list of github page domains

        Classe ``GetDomains`` of the module ``dh``
        ==========================================

        Retrieves a list of github related domains from :
            - hackertarget's reverse lookup ip API
            - A file with one domain per line
            - CNAME file search on github (to implement)

        :Example of package class import:

        >>> from dh import GetDomains
        >>> d = GetDomains(self.read_conf_file(), "my_awesome_domains.list")
        >>> domains = d.domains
    """
    def __init__(self, config, get_domains_method="", in_file=""):
        """
            This is the constructor of the GetDomains class

            Configures class attributes

            Args:
                :param self: Pending instance
                :type self: GetDomains object

                :param config: Config file
                :type config: ConfigParser object

                :param in_file: Path of the domains list file
                :type in_file: str
        """
        try:
            # Load github conf vars
            self.github_ips = [v.strip() for v in config.get('SETTINGS', 'github_ips').split(',')]
            self.github_user = config.get('SETTINGS', 'github_user')
            self.github_password = config.get('SETTINGS', 'github_password')
            self.github_access_token = config.get('SETTINGS', 'github_access_token')
            # Load hackertarget conf vars
            self.rvs_ip_lkp_api_url = config.get('SETTINGS', 'reverse_ip_lookup_api_url')
            self.rvs_ip_lkp_api_key = config.get('SETTINGS', 'reverse_ip_lookup_api_key')
        except ImportError:
            logging.error("Unable to parse the config file")
            if not in_file:
                logging.error("Unable to continue without a configuration file or domain list to scan.")
                sys.exit(1)
        # Recuperation domains method : github, file, reverse_lookup, all
        if get_domains_method == "file":
            if in_file:
                domains_list = self.read_domains_in_file(in_file)
                self.domains = self.remove_duplicates(domains_list)
        elif get_domains_method == "github":
            domains_list = self.get_domains_with_github_commits()
            self.domains = self.remove_duplicates(domains_list)
        elif get_domains_method == "reverse_lookup":
            domains_list = self.get_domain_with_reverse_lookup_ip()
            self.domains = self.remove_duplicates(domains_list)
        else:
            if in_file:
                domains_list_1 = self.read_domains_in_file(in_file)
                domains_list_2 = self.get_domains_with_github_commits()
                domains_list_3 = self.get_domain_with_reverse_lookup_ip()
                domains_list = domains_list_1 + domains_list_2 + domains_list_3
                self.domains = self.remove_duplicates(domains_list)
            else:
                domains_list_1 = self.get_domains_with_github_commits()
                domains_list_2 = self.get_domain_with_reverse_lookup_ip()
                domains_list = domains_list_1 + domains_list_2
                self.domains = self.remove_duplicates(domains_list)

    @staticmethod
    def read_domains_in_file(in_file):
        """
            Reads the file that contains the domains and places the domains in a list.

            Args:
                :param in_file: Path of the domains list file
                :type in_file: str

            Returns:
                :return: List with each line of the in_file
                :rtype: list
        """
        domains = []
        # open file and read the content in a list
        with open(in_file, 'r') as file:
            for line in file:
                # remove linebreak which is the last character of the string
                domain = line[:-1]
                # add item to the list
                domains.append(domain)
        return domains

    @staticmethod
    def remove_duplicates(domains_list):
        """
            Removes duplicates in a list.

            Args:
                :param in_file: Domains list
                :type in_file: list

            Returns:
                :return: List of domains without duplicates
                :rtype: list
        """
        return list(OrderedDict.fromkeys(domains_list))

    def get_domain_with_reverse_lookup_ip(self):
        """
            Recover domains with hackertarget api.

            Args:
                :param self: Pending instance
                :type self: GetDomains object

            Returns:
                :return: List of domains
                :rtype: list
        """
        result = ""
        for ip_address in self.github_ips:
            if self.rvs_ip_lkp_api_key:
                api_url = '{0}{1}&apikey={2}'.format(self.rvs_ip_lkp_api_url, ip_address, self.rvs_ip_lkp_api_key)
            else:
                api_url = '{0}{1}'.format(self.rvs_ip_lkp_api_url, ip_address)

            response = requests.get(api_url)

            if response.status_code == 200:
                domains = '{0}{1}'.format(response.content.decode('utf-8'), "\n")
                result += domains
            else:
                logging.error('API has issued an incorrect return code. Return code : %s', response.status_code)
                return None
        return [v.strip() for v in result.split('\n')]

    def get_domains_with_github_commits(self):
        """
            Recover domains with github commits.

            Args:
                :param self: Pending instance
                :type self: GetDomains object

            Returns:
                :return: List of domains
                :rtype: list
        """
        # Create a Github instance:
        if self.github_access_token:
            github = Github("access_token")
        elif self.github_user and self.github_password:
            github = Github(self.github_user, self.github_password)
        else:
            github = Github()

        queue = Queue()
        threads = []

        # Then search CNAME commit
        try:
            commits = github.search_commits(query="create CNAME", sort="committer-date", order="desc")
        except EnvironmentError as error:
            logging.error("Unable to perform the github query : %s", error)
        else:
            for commit in commits:
                try:
                    commit_raw_url = commit.raw_data["files"][0]['raw_url']
                except RateLimitExceededException as rle_except:
                    logging.error("Unable to get raw URL from github : %s", rle_except)
                    return []
                else:
                    my_thread = Thread(target=self.get_domain_with_commit_link, args=(commit_raw_url, queue))
                    my_thread.start()
                    threads.append(my_thread)

            for thread in threads:
                thread.join()

            return [queue.get() for _ in range(len(commits))]
        return []

    def get_domain_with_commit_link(self, commit_raw_url, queue):
        """
            Method that runs forever

            Get domain from raw commit

            Args:
                :param self: Pending instance
                :type self: GetDomains object

                :param commit_raw_url: Commit url
                :type commit_raw_url: str

                :param queue: Queue for download domains
                :type queue: Queue object
        """
        # Get domains from commit_raw_url
        session = requests.Session()
        retry = Retry(total=2, connect=2, redirect=2, backoff_factor=0.5)
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        response = session
        try:
            response = session.get(commit_raw_url, timeout=2)
        except ConnectionError as connection_error:  # there are some other exceptions I want to ignore
            logging.error('Fatal error: {}. Error description : %s',
                          (connection_error.__class__, str(connection_error)))
        else:
            if response.status_code == 200:
                if self.is_valid_domain(response.text):
                    queue.put(response.text)
                else:
                    logging.error('Wrong domain syntax for %s', response.text)
            else:
                logging.error('Wrong status code : %s', response.status_code)

    @staticmethod
    def is_valid_domain(raw_domain):
        """
            Check for correct domain syntax

            Get domain from raw commit

            Args:
                :param raw_domain: Url to check
                :type raw_domain: str

            Returns:
                :return: True if valid domain syntax
                :rtype: Boolean
        """
        pattern = r'^([A-Za-z0-9]\.|[A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9]\.){1,3}[A-Za-z]{2,6}$'
        if not re.match(pattern, raw_domain):
            return False
        return True


class GetChunk(Thread):
    """
        .. module:: dh
        .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

        :platform: Unix, Windows
        :synopsis: Downloads the page content from a domain

        Classe ``GetChunk`` of the module ``dh``
        ========================================

        Provides methods to retrieve the raw HTML page of a domain in a thread.
        Retrieves the domain to be downloaded from the queue and places its raw content into another queue.

        :Example of package class import:

        >>> from dh import GetChunk
        >>> t = GetChunk('Thread_GetChunk_%s' % i, queue, out_queue)
        >>> t.daemon = True
        >>> t.start()
    """
    def __init__(self, name, queue, out_queue):
        """
            This is the constructor of the GetChunk class

            Configures class attributes

            Args:
                :param self: Pending instance
                :type self: GetChunk object

                :param name: GetChunk object name
                :type name: str

                :param queue: Queue with domains
                :type queue: Queue object

                :param out_queue: Queue with raw source code
                :type out_queue: Queue object
        """
        Thread.__init__(self)
        self.name = name
        self.queue = queue
        self.out_queue = out_queue

    def run(self):
        """
            Method that runs forever

            Download raw sources from domain

            Args:
                :param self: Pending instance
                :type self: GetChunk object
        """
        while True:
            logging.debug('running thread: %s', self.name)
            # Grabs domain from queue
            domain = self.queue.get()

            # Grabs url of domain and then grabs chunk of webpage
            session = requests.Session()
            retry = Retry(total=2, connect=2, redirect=2, backoff_factor=0.5)
            adapter = HTTPAdapter(max_retries=retry)
            session.mount('http://', adapter)
            session.mount('https://', adapter)
            response = session
            try:
                response = session.get("http://"+domain, timeout=2)
            except ConnectionError as connection_error:  # there are some other exceptions I want to ignore
                logging.error('Fatal error: {}. Error description : %s',
                              (connection_error.__class__, str(connection_error)))
            else:
                if response.status_code == 404:
                    chunk = response.text

                    # Place chunk into out queue
                    self.out_queue.put((domain, chunk))

            # Signals to queue job is done
            self.queue.task_done()


class TestDomainIsHijackable(Thread):
    """
        .. module:: dh
        .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

        :platform: Unix, Windows
        :synopsis: Check that 404 error page is the one from github

        Classe ``TestDomainIsHijackable`` of the module ``dh``
        =======================================================

        Verifies that the source code in the queue contains the github 404 error code text.
        If it does, places the domain in another queue

        :Example of package class import:

        >>> from dh import TestDomainIsHijackable
        >>> t = TestDomainIsHijackable('Thread_TestDomainIsHijackable_%s' % i, out_queue, hijackable_queue)
        >>> t.daemon = True
        >>> t.start()
    """
    def __init__(self, name, out_queue, hijackable_queue):
        """
            This is the constructor of the TestDomainIsHijackable class

            Configures class attributes

            Args:
                :param self: Pending instance
                :type self: TestDomainIsHijackable object

                :param name: TestDomainIsHijackable object name
                :type name: str

                :param out_queue: Queue with raw source code
                :type out_queue: Queue object

                :param hijackable_queue: Queue with hijackable domains
                :type hijackable_queue: Queue object
        """
        Thread.__init__(self)
        self.name = name
        self.out_queue = out_queue
        self.hijackable_queue = hijackable_queue

    def run(self):
        """
            Method that runs forever

            Check that the source code contains the github 404 error code text

            Args:
                :param self: Pending instance
                :type self: TestDomainIsHijackable object
        """
        while True:
            # Grabs chunk from queue
            result = self.out_queue.get()
            domain = result[0]
            chunk = result[1]

            # Parse the chunk
            str_github = "<p><strong>There isn't a GitHub Pages site here.</strong></p>"
            if re.search(str_github, chunk):
                # Place hijackable domain into hijackable_queue queue
                self.hijackable_queue.put(('domain', domain))

            # Signals to queue job is done
            self.out_queue.task_done()


class TestSubdomainIsHijackable(Thread):
    """
        .. module:: dh
        .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

        :platform: Unix, Windows
        :synopsis: Check if a sub-domain also points to github's ips

        Classe ``TestSubdomainIsHijackable`` of the module ``dh``
        =========================================================

        Verifies if the sub-domains of the identified domains also point to the github ips.

        :Example of package class import:

        >>> from dh import TestSubdomainIsHijackable
        >>> t = TestSubdomainIsHijackable('Thread_TestSubdomainIsHijackable_%s' % i, out_queue, hijackable_queue)
        >>> t.daemon = True
        >>> t.start()
    """
    def __init__(self, name, config, queue, hijackable_queue):
        """
            This is the constructor of the TestSubdomainIsHijackable class

            Configures class attributes

            Args:
                :param self: Pending instance
                :type self: TestSubdomainIsHijackable object

                :param name: TestSubdomainIsHijackable object name
                :type name: str

                :param out_queue: Queue with raw source code
                :type out_queue: Queue object

                :param hijackable_queue: Queue with hijackable domains
                :type hijackable_queue: Queue object
        """
        Thread.__init__(self)
        self.name = name
        self.queue = queue
        self.hijackable_queue = hijackable_queue
        try:
            # Load github conf vars
            self.github_ips = [v.strip() for v in config.get('SETTINGS', 'github_ips').split(',')]
        except ImportError:
            logging.error("Unable to parse the config file")

    def run(self):
        """
            Method that runs forever

            Check that the source code contains the github 404 error code text

            Args:
                :param self: Pending instance
                :type self: TestSubdomainIsHijackable object
        """
        while True:
            # Grabs chunk from queue
            domain = self.queue.get()

            sub_domain_ips = pydig.query('hijack_test.{}'.format(domain), 'A')

            # check if self.github_ips contains all elements in domain_ips
            result = all(elem in self.github_ips for elem in sub_domain_ips)

            if result:
                # Place hijackable domain into hijackable_queue queue
                self.hijackable_queue.put(('sub-domain', domain))

            # Signals to queue job is done
            self.queue.task_done()


class ReturnInfosAboutHijackableWebsites(Thread):
    """
        .. module:: dh
        .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

        :platform: Unix, Windows
        :synopsis: Get information about hijackable domains

        Classe ``ReturnInfosAboutHijackableWebsites`` of the module ``dh``
        ==================================================================

        Retrieve hijackable domain information from the whois for custom domains and otherwise
        retrieve the github account link. Eventually places these infos in a queue to write them
        in a result file.

        :Example of package class import:

        >>> from dh import ReturnInfosAboutHijackableWebsites
        >>> t = ReturnInfosAboutHijackableWebsites('Thread_RIAHW_%s' % i, h_queue, country, w_queue)
        >>> t.daemon = True
        >>> t.start()
    """
    def __init__(self, name, hijackable_queue, country="", write_queue=None):
        """
            This is the constructor of the ReturnInfosAboutHijackableWebsites class

            Configures class attributes

            Args:
                :param self: Pending instance
                :type self: ReturnInfosAboutHijackableWebsites object

                :param name: ReturnInfosAboutHijackableWebsites object name
                :type name: str

                :param hijackable_queue: Queue with hijackable domains
                :type hijackable_queue: Queue object

                :param country: Country name
                :type country: str

                :param write_queue: Queue with info to write in result file
                :type write_queue: Queue object
        """
        Thread.__init__(self)
        self.name = name
        self.hijackable_queue = hijackable_queue
        self.write_queue = write_queue
        self.country = country

    def run(self):
        """
            Method that runs forever

            Get information about hijackable domains from the whois

            Args:
                :param self: Pending instance
                :type self: ReturnInfosAboutHijackableWebsites object
        """
        while True:
            # Grabs domain from queue
            hijackable_part = self.hijackable_queue.get()[0]
            domain = self.hijackable_queue.get()[1]
            domain_details = tldextract.extract(domain)
            domain_with_suffix = domain_details.registered_domain
            if domain_with_suffix == "github.io":
                if not self.country:
                    github_account = 'https://github.com/{}'.format(domain_details.subdomain)
                    info = '[{} hijackable] {} - {}'.format(hijackable_part.capitalize(), domain, github_account)
                    print(info)
                    if self.write_queue:
                        # Place line into write queue
                        self.write_queue.put(info)
            else:
                domain_whois = whois.whois(domain)
                if self.country:
                    if self.country == domain_whois.country:
                        info = '[{} hijackable] {} - {} - {} - {}'.format(hijackable_part.capitalize(),
                                                                          domain,
                                                                          domain_whois.country,
                                                                          domain_whois.registrar,
                                                                          domain_whois.emails)
                    else:
                        info = ''
                    print(info)
                    if self.write_queue:
                        # Place line into write queue
                        self.write_queue.put(info)
                else:
                    info = '[{} hijackable] {} - {} - {} - {}'.format(hijackable_part.capitalize(),
                                                                      domain,
                                                                      domain_whois.country,
                                                                      domain_whois.registrar,
                                                                      domain_whois.emails)
                    print(info)
                    if self.write_queue:
                        # Place line into write queue
                        self.write_queue.put(info)
            # Signals to queue job is done
            self.hijackable_queue.task_done()


class WriteResultToFile(Thread):
    """
        .. module:: dh
        .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

        :platform: Unix, Windows
        :synopsis: Write results in a file

        Classe ``WriteResultToFile`` of the module ``dh``
        =================================================

        Write hijackable domains with their info in a file

        :Example of package class import:

        >>> from dh import WriteResultToFile
        >>> t = WriteResultToFile('Thread_WriteResultToFile', self.out_file, write_queue)
        >>> t.daemon = True
        >>> t.start()
    """
    def __init__(self, name, out_file, write_queue):
        """
            This is the constructor of the WriteResultToFile class

            Configures class attributes

            Args:
                :param self: Pending instance
                :type self: WriteResultToFile object

                :param name: WriteResultToFile object name
                :type name: str

                :param out_file: Result file path
                :type out_file: str

                :param write_queue: Queue with info to write in result file
                :type write_queue: Queue object
        """
        Thread.__init__(self)
        self.name = name
        self.out_file = out_file
        self.write_queue = write_queue

    def run(self):
        """
            Method that runs forever

            Get informations in the queue and calls the write_line_to_file function with it.

            Args:
                :param self: Pending instance
                :type self: WriteResultToFile object
        """
        while True:
            # Grabs domain from queue
            hijackable_domain_info = self.write_queue.get()
            self.write_line_to_file(hijackable_domain_info)
            # Signals to queue job is done
            self.write_queue.task_done()

    def write_line_to_file(self, line):
        """
            Write line content to the out_file

            Args:
                :param self: Pending instance
                :type self: WriteResultToFile object

                :param line: Line to write
                :type line: str
        """
        # Open a file with access mode 'a'
        file_object = open(self.out_file, 'a')
        # Append the line at the end of file
        file_object.write(line + '\n')
        # Close the file
        file_object.close()


class HijackDomain:
    """
        .. module:: dh
        .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

        :platform: Unix, Windows
        :synopsis: Create the CNAME file

        Classe ``HijackDomain`` of the module ``dh``
        ============================================

        Generation of the CNAME file

        :Example of package class import:

        >>> from dh import HijackDomain
        >>> t = HijackDomain()
        >>> t.daemon = True
    """
    def start(self):
        """
            Manages the interactive menu to hijack domain

            Args:
                :param self: Pending instance
                :type self: HijackDomain object
        """
        menu_header = "***********************  CNAME Generator  ************************"
        menu = "Enter the domain you want to hijack\n"
        selection_error_msg = "You should enter a correct domain. Try again."
        print("\n" + menu_header)
        print(menu)
        invalid_domain = 1
        while invalid_domain:
            try:
                domain = input("Domain: ")
                if self.is_valid_domain(domain):
                    invalid_domain = 0
                else:
                    print(selection_error_msg)
            except ValueError:
                print(selection_error_msg)
        self.hijack_domain(domain)

    def hijack_domain(self, domain):
        """
            Check for correct domain syntax

            Get domain from raw commit

            Args:
                :param self: Pending instance
                :type self: GetDomains object

                :param raw_domain: Url to check
                :type raw_domain: str
        """
        filename = "CNAME"
        file_exists = os.path.isfile(filename)
        if file_exists:
            print("A CNAME file already exists. Do you want to overwrite it?")
            selection_error_msg = "yes/no are your options. Try again."
            invalid_selection = 1
            valid_choices = ["yes", "no"]
            while invalid_selection:
                try:
                    choice = input("yes/no : ")
                    if choice in valid_choices:
                        invalid_selection = 0
                    else:
                        print(selection_error_msg)
                except ValueError:
                    print(selection_error_msg)
            if choice == "yes":
                self.create_cname_file(domain)
        else:
            self.create_cname_file(domain)

    @staticmethod
    def is_valid_domain(raw_domain):
        """
            Check for correct domain syntax

            Args:
                :param raw_domain: Url to check
                :type raw_domain: str

            Returns:
                :return: True if valid domain syntax
                :rtype: Boolean
        """
        pattern = r'^([A-Za-z0-9]\.|[A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9]\.){1,3}[A-Za-z]{2,6}$'
        if not re.match(pattern, raw_domain):
            return False
        return True

    @staticmethod
    def create_cname_file(domain):
        """
            Create CNAME file

            Args:
                :param domain: Domain to write
                :type domain: str
        """
        cname_file = open("CNAME", "w")
        cname_file.write(domain)
        print("\n[+] Done")


class App:
    """
        .. module:: dh
        .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

        :platform: Unix, Windows
        :synopsis: Launches the program and interacts with users

        Classe ``App`` of the module ``dh``
        ===================================

        Write hijackable domains with their info in a file

        :Example of package class import:

        >>> from dh import WriteResultToFile
        >>> app = App(only_custom_domains=True,
        >>>           num_threads=10,
        >>>           country="CN",
        >>>           in_file="domain.txt",
        >>>           out_file="results.txt",
        >>>           conf_file="settings.ini")
        >>> app.start()
    """
    def __init__(self,
                 only_custom_domains=False,
                 num_threads=10, domains="",
                 country="",
                 in_file="",
                 out_file="",
                 conf_file=""):
        """
            This is the constructor of the App class

            Configures class attributes

            Args:
                :param self: Pending instance
                :type self: App object

                :param only_custom_domains: Displays only the custom domains
                :type only_custom_domains: boolean

                :param num_threads: Number of threads to run
                :type num_threads: int

                :param country: Country name
                :type country: str

                :param in_file: Path of the domains list file
                :type in_file: str

                :param out_file: Result file path
                :type out_file: str

                :param conf_file: Configuration file path
                :type conf_file: str
        """
        # pylint: disable=too-many-arguments
        self.only_custom_domains = only_custom_domains
        self.num_threads = num_threads
        self.country = country
        self.in_file = in_file
        self.out_file = out_file
        self.conf_file = conf_file
        self.domains = domains

    def read_conf_file(self):
        """
            Reads the configuration file

            Args:
                :param self: Pending instance
                :type self: App object

            Returns:
                :param config: Config file
                :rtype: ConfigParser object
        """
        #####################################################
        # Reading the configuration file
        #####################################################
        # 'Safe' is preferred, according to docs
        config = SafeConfigParser(inline_comment_prefixes="#")
        config.read(self.conf_file)
        return config

    def main(self):
        """
            Manage queues and launch threads

            Args:
                :param self: Pending instance
                :type self: App object
        """
        # pylint: disable-msg=too-many-locals
        queue = Queue()
        out_queue = Queue()
        subdomain_queue = Queue()
        hijackable_queue = Queue()

        num_threads = self.num_threads

        dom = GetDomains(self.read_conf_file(), self.domains, self.in_file)
        domains = dom.domains

        if self.only_custom_domains:
            domains = [val for val in domains if not val.endswith("github.io")]

        # Spawn a pool of threads, and pass them queue instance
        for i in range(num_threads):
            thread = GetChunk('Thread_GetChunk_%s' % i, queue, out_queue)
            thread.daemon = True
            thread.start()

        for i in range(num_threads):
            dthread = TestSubdomainIsHijackable('Thread_TestSubdomainIsHijackable_%s' % i,
                                                self.read_conf_file(),
                                                subdomain_queue,
                                                hijackable_queue)
            dthread.daemon = True
            dthread.start()

        # Populate queue with data
        for domain in domains:
            queue.put(domain)
            subdomain_queue.put(domain)

        for i in range(num_threads):
            sthread = TestDomainIsHijackable('Thread_TestDomainIsHijackable_%s' % i, out_queue, hijackable_queue)
            sthread.daemon = True
            sthread.start()

        if self.out_file:
            write_queue = Queue()
            # spawn threads to print
            wthread = WriteResultToFile('Thread_WriteResultToFile', self.out_file, write_queue)
            wthread.daemon = True
            wthread.start()

            for i in range(num_threads):
                hthread = ReturnInfosAboutHijackableWebsites('Thread_ReturnInfosAboutHijackableWebsites_%s' % i,
                                                             hijackable_queue,
                                                             self.country,
                                                             write_queue)
                hthread.daemon = True
                hthread.start()
        else:
            for i in range(num_threads):
                ithread = ReturnInfosAboutHijackableWebsites('Thread_RIAHW_%s' % i,
                                                             hijackable_queue,
                                                             self.country)
                ithread.daemon = True
                ithread.start()

        # Wait on the queue until everything has been processed
        queue.join()
        out_queue.join()
        subdomain_queue.join()
        hijackable_queue.join()

    def start(self):
        """
            Manages the interactive menu at launch

            Args:
                :param self: Pending instance
                :type self: App object
        """
        print(BANNER)
        selection_error_msg = "1-3 are your options. Try again."
        print(MENU)
        invalid_selection = 1
        while invalid_selection:
            try:
                choice = int(input("Selection: "))
                if 1 <= choice <= 3:
                    invalid_selection = 0
                else:
                    print(selection_error_msg)
            except ValueError:
                print(selection_error_msg)
        if choice == 1:
            self.main()
        elif choice == 2:
            domain_hijack = HijackDomain()
            domain_hijack.start()
        elif choice == 3:
            self.exit_msg()
        else:
            print(selection_error_msg)

    @staticmethod
    def exit_msg():
        """
            Displays a random exit message

            Args:
                :param self: Pending instance
                :type self: App object
        """
        bye_array = ("Bye!", "Ciao!", "Adios!", "Kenavo!", "Tsau!", "Tschuss!", "Adieu!")

        print("")
        print(random.choice(bye_array))
        print("")


if __name__ == '__main__':
    # Constant
    LOG_LEVEL = "CRITICAL"
    ONLY_CUSTOM = False
    IN_FILE = ""
    OUT_FILE = ""
    CONF_FILE = "settings.ini"
    COUNTRY = ""
    DOMAINS = "all"

    # docopt saves arguments and options as key:value pairs in a dictionary
    args = docopt(__doc__, version=VERSION)

    # Parsing command line arguments with docopt
    valid_log_level = ["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"]
    if args['--log-level']:
        if args['--log-level'].upper() in valid_log_level:
            LOG_LEVEL = args['--log-level'].upper()
    if args['--flag']:
        pass
    if args['--only-custom']:
        ONLY_CUSTOM = True
    if args['--in-file']:
        IN_FILE = args['--in-file']
    if args['--out-file']:
        OUT_FILE = args['--out-file']
    if args['--conf-file']:
        CONF_FILE = args['--conf-file']
    if args['--flag']:
        COUNTRY = args['--flag']
    valid_domains_recuperation_method = ["github", "file", "reverse_lookup", "all"]
    if args['--domains']:
        if args['--domains'] in valid_domains_recuperation_method:
            DOMAINS = args['--domains']

    # log level
    logging.getLogger('urllib3.connectionpool').setLevel(getattr(logging, LOG_LEVEL))
    logging.basicConfig(level=getattr(logging, LOG_LEVEL))
    logging.debug(args)

    app = App(only_custom_domains=ONLY_CUSTOM,
              num_threads=50,
              domains=DOMAINS,
              country=COUNTRY,
              in_file="domain.txt",
              out_file=OUT_FILE,
              conf_file=CONF_FILE)
    app.start()
