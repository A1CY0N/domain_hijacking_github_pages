"""
    Package that parses arguments given on the CLI

    .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

    .. automodule:: docopt.py
    :members:
"""
from .docopt import docopt