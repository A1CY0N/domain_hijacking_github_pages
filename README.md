# DomainHijack

> Search for domain names that can be hijacked with github pages

Github offers a service that allows you to [host static web pages for free](https://pages.github.com/) and even configure your own domain following the procedure described [here](https://docs.github.com/en/free-pro-team@latest/github/working-with-github-pages/configuring-a-custom-domain-for-your-github-pages-site). This service, while convenient for developers, also poses security issues if it is misconfigured. It is indeed possible, in two situations, to hijack a domain name and host pages for free on a domain that does not belong to us. Github has been contacted several times to correct this issue, but this does not represent a security flaw on the part of Github. It is simply a user configuration error.

There are 2 cases where you can hijack a domain name with github pages.

1 - The domain name is resolved by the IPs github.io and the CNAME file is missing in the repo. Most of the time, this means that the repository has been deleted.

2 - The user has pointed a DNS wildcard record to the IPs of github.io and in this case, we can host hacker sites using subdomains.

## Requirements

* Python 3.x
* Libraries
  * python-whois
  * tldextract
  * pyGithub
  * pydig

Check ``requirements.txt`` for complete list

## Installation

### Pre-requisite
In order to optimize the number of results, it makes sense to use a github account and a hackertarget account.
You can then fill in the ``settings.ini`` file :
```
# Use github username and password
github_user=YourGitHubUsername
github_password=YourGitHubPassword
# Or use an access token
github_access_token=YourGitHubToken

# Without API Key, results are limited to 500
reverse_ip_lookup_api_key=YourAPIKey
```

### OS X & Linux:

1. Download DomainHijack
    ```sh
    git clone https://gitlab.com/A1CY0N/domain_hijacking_github_pages.git
    cd domain_hijacking_github_pages/src
    ```

2. Download dependencies
    ```sh
    virtualenv venv && source venv/bin/activate && pip install -r requirements.txt
    ```

3. Launch DomainHijack
    ```sh
    python3 dh.py -h
    ```
## Usage
Find out if the domains in domains.list are hijackable. The list of hijackable domains will be put in the hijackable.list file.
```
$ python3 dh.py -i domains.list -o hijackable.list
```

Search the list of US hijackable custom domains.
```
$ python3 dh.py -x -f US
```

You can consult the documentation [here](https://a1cy0n.gitlab.io/domain_hijacking_github_pages)

## Release History

* 0.1.0
    * The first proper release
    * CHANGE: README.md
    * ADD: src/dh.py
* 0.1.1
    * Update: Source code documentation
* 0.1.2
    * Update: Fix PEP8

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/domain_hijacking_github_pages>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request