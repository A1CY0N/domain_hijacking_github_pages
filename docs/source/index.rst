DomainHijack documentation
==========================

.. warning:: The code is provided **as is**, without any guarantee and must not be used for malicious purposes.

Context
-------
TL;DR
~~~~~
DomainHijack is a tool I created in python3 to search for domains vulnerable to hijacks. 

Explanations
~~~~~~~~~~~~
Github offers a service that allows you to host static web pages for free and even configure your own domain following the procedure described here. This service, while convenient for developers, also poses security issues if it is misconfigured. It is indeed possible, in two situations, to hijack a domain name and host pages for free on a domain that does not belong to us. Github has been contacted several times to correct this issue, but this does not represent a security flaw on the part of Github. It is simply a user configuration error.

.. note:: 
   There are 2 cases where you can hijack a domain name with github pages :
   
   1. The domain name is resolved by the IPs github.io and the CNAME file is missing in the repo. Most of the time, this means that the repository has been deleted.
   
   2. The user has pointed a DNS wildcard record to the IPs of github.io and in this case, we can host hacker sites using subdomains.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   install
   usage
   dh