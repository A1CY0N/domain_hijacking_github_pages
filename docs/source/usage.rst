Usage
=====
Find out if the domains in domains.list are hijackable. The list of hijackable domains will be put in the hijackable.list file.
::
    $ python3 dh.py -i domains.list -o hijackable.list


Search the list of German hijackable custom domains.
::
    $ python3 dh.py -x -f Germany