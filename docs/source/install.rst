Install
=======
Pre-requisite
-------------
In order to optimize the number of results, it makes sense to use a github account and a hackertarget account.
You can then fill in the ``settings.ini`` file ::

    # Use github username and password
    github_user=YourGitHubUsername
    github_password=YourGitHubPassword
    # Or use an access token
    github_access_token=YourGitHubToken

    # Without API Key, results are limited to 500
    reverse_ip_lookup_api_key=YourAPIKey

OS X & Linux
------------

1. Download DomainHijack::
    git clone https://gitlab.com/A1CY0N/domain_hijacking_github_pages.git
    cd domain_hijacking_github_pages/src

2. Download dependencies::
    virtualenv venv && source venv/bin/activate && pip install -r requirements.txt

3. Launch DomainHijack::
    python3 dh.py -h